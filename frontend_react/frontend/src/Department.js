import React, { Component } from "react";
import { Table } from 'react-bootstrap';

import {Button,ButtonToolbar} from 'react-bootstrap';
import AddDepModal from './AddDepModal';
import EditDepModal from './EditDepModal';




export class Department extends Component {

    constructor(props) {
        super(props);
        this.state = { deps:[], addModalShow:false, editModalShow:false};
    }

    refreshList() {
        fetch(process.env.REACT_APP_API + 'department')
            .then(response => response.json())
            .then(data => {
                this.setState({ deps: data });

            })
            .catch(error => {
                console.error('Error fetching department data:', error);
            });

    }

    componentDidMount() {
        this.refreshList();
    }

    deleteDep(depid){
        if(window.confirm('Are you sure?')){
            fetch(process.env.REACT_APP_API+'department/'+depid,{
                method:'DELETE',
                headers:{'Accept':'application/json',
            'content-type':'application/json'}
            })
        }

    }

    render() {
        const { deps,depid,depname } = this.state;
        let addModalClose=()=>this.setState({addModalShow:false});
        let editModalClose=()=>this.setState({editModalShow:false});
        return (
            <div>
                <Table striped bordered hover>
                    <thead>
                        <tr>
                            <th>Department ID</th>
                            <th>Department Name</th>
                            <th>Options</th>
                        </tr>
                    </thead>
                    <tbody>
                        {deps.map(dep =>
                            <tr key={dep.id}>
                                <td>{dep.id}</td>
                                <td>{dep.departmentname}</td>
                                <td>
            <ButtonToolbar>
                <Button className="me-2" variant="info"
                 onClick={()=>this.setState({editModalShow:true,
                    depid:dep.id,
                    depname:dep.departmentname})}>
                    Edit
                </Button>


                <Button className="ms-2" variant="danger"
                 onClick={()=>this.deleteDep(dep.id)}>
                    Delete
                </Button>

                <EditDepModal show={this.state.editModalShow}
                onHide={editModalClose}
                depid={depid}
                depname={depname}/>

            </ButtonToolbar>


                                </td>

                            </tr>
                        )}
                    </tbody>
                </Table>

                <ButtonToolbar>

                    <Button variant="primary"
                    onClick={()=>this.setState({addModalShow:true})}>
                        Add Department </Button>
                    
                    <AddDepModal show={this.state.addModalShow} 
                    onHide={addModalClose}/>
                         


                </ButtonToolbar>

            </div>
        )
    }
}

export default Department;
