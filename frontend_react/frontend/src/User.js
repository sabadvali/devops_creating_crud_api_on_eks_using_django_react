import React, { Component } from "react";
import { Table } from 'react-bootstrap';

import {Button,ButtonToolbar} from 'react-bootstrap';
import AddUsrModal from './AddUsrModal';
import EditUsrModal from './EditUsrModal';




export class User extends Component {

    constructor(props) {
        super(props);
        this.state = { usrs:[], addModalShow:false, editModalShow:false};
    }

    refreshList() {        
        fetch(process.env.REACT_APP_API + 'user')
            .then(response => response.json())
            .then(data => {
                this.setState({usrs: data });

            })
            .catch(error => {
                console.error('Error fetching user data:', error);
            });

    }
    

    componentDidMount() {
        this.refreshList();
    }

    deleteUsr(usrid){
        if(window.confirm('Are you sure?')){
            fetch(process.env.REACT_APP_API+'user/'+usrid,{
                method:'DELETE',
                headers:{'Accept':'application/json',
            'content-type':'application/json'}
            })
        }

    }

    render() {
        const { usrs,usrid,usrname,usrlname,usrdep,usrage,usrpid } = this.state;
        let addModalClose=()=>this.setState({addModalShow:false});
        let editModalClose=()=>this.setState({editModalShow:false});
        return (
            <div>
                <Table striped bordered hover>
                    <thead>
                        <tr>
                            <th>User ID</th>
                            <th>User Name</th>
                            <th>Last Name</th>
                            <th>Age</th>
                            <th>Department</th>
                            <th>Identity Number</th>
                            <th>Options</th>
                        </tr>
                    </thead>
                    <tbody>
                        {usrs.map(use =>
                            <tr key={use.id}>
                                <td>{use.id}</td>
                                <td>{use.first_name}</td>
                                <td>{use.last_name}</td>
                                <td>{use.age}</td>
                                <td>{use.user_department}</td>
                                <td>{use.user_id}</td>
                                <td>
            <ButtonToolbar>
                <Button className="me-2" variant="info"
                 onClick={()=>this.setState({editModalShow:true,
                    usrid:use.id,
                    usrname:use.first_name,
                    usrlname:use.last_name,
                    usrage:use.age,
                    usrdep:use.user_department,
                    usrpid:use.user_id,
                    })}>
                    Edit
                </Button>


                <Button className="ms-2" variant="danger"
                 onClick={()=>this.deleteUsr(use.id)}>
                    Delete
                </Button>

                <EditUsrModal show={this.state.editModalShow}
                onHide={editModalClose}
                usrid={usrid}
                usrname={usrname}
                usrlname={usrlname}
                usrage={usrage}
                usrdep={usrdep}
                usrpid={usrpid}              
                />

            </ButtonToolbar>


                                </td>

                            </tr>
                        )}
                    </tbody>
                </Table>

                <ButtonToolbar>

                    <Button variant="primary"
                    onClick={()=>this.setState({addModalShow:true})}>
                        Add User </Button>
                    
                    <AddUsrModal show={this.state.addModalShow} 
                    onHide={addModalClose}/>
                         


                </ButtonToolbar>

            </div>
        )
    }
}

export default User;
