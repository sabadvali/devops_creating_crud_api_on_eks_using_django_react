// import logo from './logo.svg';
import './App.css';

import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Home from './Home';
import Department from './Department';
import User from './User';
import Navigation from './Navigation';
import 'bootstrap/dist/css/bootstrap.min.css';



function App() {
  return (
    <BrowserRouter>
      <div className="container">
        <h3 className='m-3 d-flex justify-content-center'> 
          Full Stack Web Application With a DevOps Mindset
        </h3>

        <Navigation/>
        <Routes>
          
            <Route path='/' element={<Home />} exact="true" />
            <Route path='/department' element={<Department />} />
            <Route path='/user' element={<User />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
