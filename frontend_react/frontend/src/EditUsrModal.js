import React,{Component} from "react";
import { Modal,Button,Row,Col,Form } from "react-bootstrap";


export class EditusrModal extends Component{
    constructor(props){
        super(props);
        this.state={usrs:[]};
        this.state={deps:[]};
        this.handleSubmit=this.handleSubmit.bind(this);

    }

    componentDidMount(){
        fetch(process.env.REACT_APP_API+'department')
        .then(response=>response.json())
        .then(data=>{
            this.setState({deps:data}) 
        })
    }

    handleSubmit(event){
        event.preventDefault();
        fetch(process.env.REACT_APP_API+'user/',{
            method:'PUT',
            headers:{
                'Accept':'application/json',
                'Content-Type':'application/json'
            },
            body:JSON.stringify({
                id:event.target.id.value,
                first_name:event.target.first_name.value,
                last_name:event.target.last_name.value,
                age:event.target.age.value,
                user_department:event.target.user_department.value,
                user_id:event.target.user_id.value
            })
        })
        .then(res=>res.json())
        .then((result)=>{
            alert(result);
        },
        (error)=>{
            alert("Failed");
        })
    }
    render(){
        return(
            <div className="container">

    <Modal
    {...this.props}
    size="lg"
    aria-labelledby="contained-modal-title-vcenter"
    centered
    >
    <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
                Edit User
            </Modal.Title>
    </Modal.Header>
   
    <Modal.Body>
        <Row>
            <Col sm={6}>
                <Form onSubmit={this.handleSubmit}>

                    <Form.Group controlId="id">
                        <Form.Label>id</Form.Label>
                        <Form.Control type="text" name="id" required  
                        disabled
                        defaultValue={this.props.usrid}
                        placeholder="id"/>
                    </Form.Group>

                    <Form.Group controlId="first_name">
                        <Form.Label>User Name</Form.Label>
                        <Form.Control type="text" name="first_name" required placeholder="User Name"/>
                    </Form.Group>

                    <Form.Group controlId="last_name">
                        <Form.Label>Last Name</Form.Label>
                        <Form.Control type="text" name="last_name" required placeholder="Last Name"/>
                    </Form.Group>


                    <Form.Group controlId="age">
                        <Form.Label>Age</Form.Label>
                        <Form.Control type="number" name="age" required placeholder="Age" min="0" max="120" />
                    </Form.Group>

                    <Form.Group controlId="user_department">
                        <Form.Label>Department</Form.Label>
                        <Form.Control as="select" >
                        {this.state.deps.map(dep=>
                         <option key={dep.id}>{dep.departmentname} </option> )}
                         
                         </Form.Control>

                    </Form.Group>


                    <Form.Group controlId="user_id">
                        <Form.Label>Identity Number</Form.Label>
                        <Form.Control type="texr" name="user_id" required placeholder="Identity Number"/>

                    </Form.Group>

                    <Form.Group>
                        <Button variant="primary" type="submit">
                            Edit USer 
                        </Button>

                    </Form.Group>

                </Form>
            
            </Col>
        </Row>
    </Modal.Body>

    <Modal.Footer>

            <Button variant="danger" onClick={this.props.onHide}>Close</Button>

    </Modal.Footer>



</Modal>

            </div>
        )
    }
}

export default EditusrModal;
