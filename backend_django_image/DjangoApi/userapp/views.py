from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from django.http import JsonResponse

from userapp.models import User,Departments
from userapp.serializers import UserSerializer, DepartmentSerializer


@csrf_exempt
def departmentApi(request, pk=None):
    if request.method == 'GET':
        departments = Departments.objects.all()
        departments_serializer =  DepartmentSerializer(departments, many=True)
        return JsonResponse(departments_serializer.data, safe=False)
           
    if request.method == 'POST':
        department_data = JSONParser().parse(request)
        
        department_serializer = DepartmentSerializer(data=department_data)
        
        if department_serializer.is_valid():
            department_serializer.save()  # Save the validated data
            return JsonResponse("added successfully", safe=False)
        else:
            print("Validation errors:", department_serializer.errors)  # Debug print
            return JsonResponse("failed to add.", safe=False)
    
    elif request.method == 'PUT':
        department_data = JSONParser().parse(request)
        department = Departments.objects.get(id=department_data['id'])
        department_serializer = DepartmentSerializer(department,data=department_data)
        if department_serializer. is_valid():
            department_serializer.save()
            return JsonResponse("updated successfully", safe=False)
        else:
            return JsonResponse("failed to update", safe= False)
        
    elif request.method == 'DELETE':
        department = Departments.objects.get(id=pk)
        print("movida ", department)
        department.delete()
        return JsonResponse("Deleted Succeffully", safe=False)





@csrf_exempt
def userApi(request, pk=None):
    if request.method == 'GET':
        user_data = User.objects.all()
        user_serializer =  UserSerializer(user_data, many=True)
        return JsonResponse(user_serializer.data, safe=False)
           
    if request.method == 'POST':
        user_data = JSONParser().parse(request)
        
        user_serializer = UserSerializer(data=user_data)
        
        if user_serializer.is_valid():
            user_serializer.save()  # Save the validated data
            return JsonResponse("added successfully", safe=False)
        else:
            print("Validation errors:", user_serializer.errors)  # Debug print
            return JsonResponse("failed to add.", safe=False)
    
    elif request.method == 'PUT':
        user_data = JSONParser().parse(request)
        user = User.objects.get(id=user_data['id'])
        user_serializer = UserSerializer(user,data=user_data)
        if user_serializer. is_valid():
            user_serializer.save()
            return JsonResponse("updated successfully", safe=False)
        else:
            return JsonResponse("failed to update", safe= False)
        
    elif request.method == 'DELETE':
        user_data = User.objects.get(id=pk)
        user_data.delete()
        return JsonResponse("Deleted Succeffully", safe=False)
