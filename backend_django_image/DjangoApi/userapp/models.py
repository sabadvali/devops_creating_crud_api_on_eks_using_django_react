from django.db import models


class Departments(models.Model):
    departmentname = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'departments'


class User(models.Model):
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    age = models.IntegerField()
    user_department = models.CharField(max_length=255)
    user_id = models.CharField(max_length=255,unique=True)

    class Meta:
        managed = False
        db_table = 'user'
