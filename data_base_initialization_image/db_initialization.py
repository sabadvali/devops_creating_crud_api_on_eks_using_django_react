import mysql.connector

host = 'mysql-cluster-ip'
port = '1433' 
user = 'root'
password = 'devopscrud'
database = 'DB' 

try:
    connection = mysql.connector.connect(
        host=host,
        port=port,
        user=user,
        password=password
    )

    if connection.is_connected():
        print('Connected to MySQL server')

        cursor = connection.cursor()

        cursor.execute(f"CREATE DATABASE IF NOT EXISTS {database}")

        cursor.execute(f"USE {database}")

        create_table_query = """
        CREATE TABLE IF NOT EXISTS user (
            id INT AUTO_INCREMENT PRIMARY KEY,
            first_name VARCHAR(255) NOT NULL,
            last_name VARCHAR(255) NOT NULL,
            age INT NOT NULL,
            user_department VARCHAR(255) NOT NULL,
            user_id VARCHAR(255) UNIQUE NOT NULL
        )
        """
        
        create_table_query2 = """
        CREATE TABLE IF NOT EXISTS departments (
            id INT AUTO_INCREMENT PRIMARY KEY,
            departmentname VARCHAR(255) NOT NULL
        )
        """

        cursor.execute(create_table_query)
        cursor.execute(create_table_query2)

        connection.commit()

        print(f"Database '{database}' and table 'initial_table' created successfully")

        cursor.close()

except mysql.connector.Error as e:
    print(f'Error connecting to MySQL server: {e}')

finally:
    if 'connection' in locals() and connection.is_connected():
        connection.close()
        print('MySQL server connection closed')



